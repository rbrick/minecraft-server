package me.rbrickis.minecraft.server;

import me.rbrickis.minecraft.server.impl.MinecraftServer;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class Main {

    public static ScheduledExecutorService TIMER = Executors.newScheduledThreadPool(4);

    public static void main(String... args) {
        MinecraftServer server = new MinecraftServer(25565);
        server.getEventBus().register(new PacketListener());
        server.start();
    }
}
