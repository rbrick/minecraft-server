package me.rbrickis.minecraft.server.config;

import com.google.gson.JsonObject;

public class ConfigurationSection {

    private JsonObject object;
    private ConfigurationSection parent;

    public ConfigurationSection(JsonObject object) {
        this(object, null);
    }

    public ConfigurationSection(JsonObject object, ConfigurationSection parent) {
        this.object = object;
        this.parent = parent;
    }

    public boolean has(String key) {
        return object.get(key).getAsBoolean();
    }

    public int getInt(String key) {
        return object.get(key).getAsInt();
    }

    public double getDouble(String key) {
        return object.get(key).getAsDouble();
    }


}
