package me.rbrickis.minecraft.server.packet.serverbound.handshake;

import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;
import me.rbrickis.minecraft.server.netty.BufferUtils;
import me.rbrickis.minecraft.server.packet.*;


@Getter
@Setter
@PacketInfo(
    info = "http://wiki.vg/Protocol#Handshake",
    direction = Direction.SERVERBOUND,
    state = State.HANDSHAKE,
    id = 0x00)
public class HandshakePacket extends ServerboundPacket {

    private int protocol;
    private String address;
    private int port;
    private State nextState;

    @Override
    public void decode(ByteBuf buf) {
        this.protocol = BufferUtils.readVarInt(buf);
        this.address = BufferUtils.readString(buf);
        this.port = buf.readUnsignedShort();
        this.nextState = State.fromInteger(BufferUtils.readVarInt(buf));
    }

}
